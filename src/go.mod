module gitlab.com/ProjectConcord/cc-priority-queue

go 1.15

require (
	github.com/arangodb/go-driver v0.0.0-20201202080739-c41c94f2de00
	github.com/bitwurx/jrpc2 v0.0.0-20200508153510-d8310ad1baf0
)
