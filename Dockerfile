FROM golang:1.15-alpine as builder
WORKDIR /go/src/cc-priority-queue

RUN apk update && apk add ca-certificates && \
    apk add git && apk add gcc && apk add libc-dev \
    && rm -rf /var/cache/apk/*

COPY src .
RUN go get -v -t ./... && GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -installsuffix cgo -o main

FROM scratch as prod
WORKDIR /
COPY --from=builder /etc/ssl/certs/ca-certificates.crt \
  /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /go/src/cc-priority-queue/main .
CMD ["/main"]
